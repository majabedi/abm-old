# Created by Majid Abedi(majabedi@gmail.com) on 12/03/2020
# This file is intended to have an agent object. It stores all the information about one agent.
# Each person includes: a status and a node.

from enum import Enum

#All the possible status of one person
class Status(Enum):
    Susceptible = 0
    Carrier = 1
    Infected = 2
    Recovered_C = 3
    Recovered_I = 6
    Dead = 4
    Exposed = 5


class person(object):
    """
    A class used to store all the information about one agent.
    """

    def __init__(self, status, n, incubation_time,age):
        """

        :param status: the status of the agent in the form of an Status.
        :param n: node of the agent, where it is situated.
        :param incubation_time: is the time that a person gets carrier but cannot transfer the infection to the others.
        :param age: age of the person.
        """
        self.status = status
        self.node = n
        self.time_current_node = 0
        self.time_passed_in_carrier_state = 0
        self.incubation_time = incubation_time
        self.age = age
        self.individualized_nodes = {}

    def get_status(self):
        """

        :return: the status of the agent
        """
        return self.status

    def set_status(self, new_status):
        """

        :param new_status: sets the status of agent.
        :return:
        """
        self.node.one_changes_status(self.status, new_status)
        self.status = new_status
        return


    def get_node(self):
        """

        :return: the current node.
        """
        return self.node


    def get_time_passed_current_node(self):
        """

        :return: the time that the agent has been on the current node.
        """
        return self.time_current_node


    def set_time_passed_current_node(self, t):
        """

        :param t: change the time passed in the current node
        :return:
        """
        self.time_current_node = t
        return


    def increase_time_passed_current_node(self, delta_t):
        """

        :param delta_t: increases the time passes in the current node by delta_t.
        :return:
        """
        self.time_current_node += delta_t
        return


    def get_age(self):
        """

        :return: the age
        """
        return self.age


    def set_node(self, node):
        """

        :param node: the new node
        :return:
        """
        self.time_current_node=0
        self.node = node
        return


    def interact_with_population(self,delta_t):
        """
        In this function all the infection interactions with the population on the same node happes.

        :param delta_t: the time step.
        :return:
        """
        #This is an exception for Exposed state
        #RULE-1:
        if (self.status==Status.Exposed):
            if(self.time_passed_in_carrier_state<self.incubation_time):
                self.time_passed_in_carrier_state+=delta_t
                return
            else:
                self.set_status(Status.Carrier)
                return

        new_status = self.node.next_markov_state(self.status,delta_t)
        if(new_status==Status.Carrier):
            new_status=Status.Exposed
        if(new_status==self.status):
            return
        self.set_status(new_status)
        return


    def migrate_to(self,destination):
        """
        In this the migration on the nodes happens

        :param destination: the destination node
        :return:
        """
        self.node.change_one_subpopulation_and_total(self.status, -1)
        destination.change_one_subpopulation_and_total(self.status, +1)
        self.set_node(destination)
        return


    def set_individualized_nodes(self, inodes):
        """

        :param inodes: sets the individualized node. It should be a map from node name to a node obejct.
        """
        self.individualized_nodes = inodes;


    def get_individualized_nodes(self):
        """

        :return: returns the individualized nodes dictionary.
        """
        return self.individualized_nodes;


