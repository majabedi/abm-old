# Created by Majid Abedi(majabedi@gmail.com) on 12/03/2020
# This file is intended to prepare the simulation and run it. It is an interface between the main runner code
# and the dynamical model.

import matplotlib.pyplot as plt
import pandas as pd

import world
from person import Status


def drawplot(df, node_name):
    ax = plt.gca()
    for status in Status:
        if(status==Status.Carrier):
            dummydf=df.copy()
            dummydf['CE'] = df[node_name + '_' + status.name]+df[node_name + '_' + Status.Exposed.name]
            dummydf.plot(kind='line', y= 'CE' , ax=ax, label=status.name)
            continue
        if(status!=Status.Exposed):
            df.plot(kind='line', y=node_name + '_' + status.name, ax=ax, use_index=True, label=status.name)
    df.plot(kind='line', y = node_name + '_total', ax=ax, use_index=True, label='Total')
    filename = node_name + ".png"
    plt.xlabel('Time')
    plt.ylabel('Number of cases')
    plt.title(node_name)
    plt.savefig(filename)
    plt.close('all')


def draw_graph(G):
    from smallworld.draw import draw_network
    ax = plt.gca()
    focal_node = 0
    k_over_2 = 2
    draw_network(G, k_over_2, focal_node=focal_node, ax=ax)
    filename = "graph.png"
    plt.title('Connection graph')
    plt.savefig(filename)
    plt.close('all')


class simulator:
    """
        A class used to create an interface to the model and run the simulation. It creates the plots and reports.
    """

    def __init__(self, world=None):
        """
        initializes the Simulator.
        :param world: if a world object already exists.

        """
        self.world = world
        self.print_graphs = False
        self.report_contacts_flag = False
        self.age_stratified_ICU_Hospital = False
        self.report_statistics_new_cases = False
        self.report_every_n_steps = 1
        self.count_down_to_next_report = 0


    def initialize_network(self, size=10, infection_p=0.2, capacity=100):
        """
        initialize a network for more theoretical works.

        :param size: size of population in each node of the world.
        :param infection_p: the infection probability
        :param capacity: the initial capacity of each node
        :return:
        """
        self.world = world.world()
        places=["Hospital","School","Work","Event","Supermarket"]
        self.world.initialize_network_complete_graph(places, size, infection_p,capacity)
        return

    def initialize_network_explicit_nodes(self, network_file, population_parameters, markov_parameters ):
        """
        initialize a network with node names

        :param network_file: input network file
        :param population_parameters: parameters to create the initial populations
        :param markov_parameters: the markov processes parameters.
        :return:
        """
        self.world = world.world()
        self.world.initialize_network_complete_graph_from_file_individualized_home(network_file=network_file,population_parameters=population_parameters,markov_parameters=markov_parameters)
        return


    def with_output(self, file):
        """

        :param file: set the main output file
        """
        self.filename = file
        return

    def print_graphs_at_the_end(self):
        """
        forces the Simulator to print the graphs
        """
        self.print_graphs = True
        return

    def report_contacts(self):
        """
        forces the Simulator to report the number of contacts.
        :return:
        """
        self.report_contacts_flag=True
        return

    def report_new_cases(self):
        """
        forces the Simulator to report the number of new cases
        :return:
        """
        self.report_statistics_new_cases=True
        return

    def report_stratified_ICU_Hospital(self):
        """
        forces the Simulator to report the age stratified ICU and Hospital agents.
        :return:
        """
        self.age_stratified_ICU_Hospital=True
        return

    def set_report_every_n_steps(self, n):
        """
        sets the frequency of reporting statistics.
        :param n: every n time steps
        """
        self.report_every_n_steps = n
        return

    def run(self, initial_time, total_time, delta_t):
        """
        runs the simulation
        :param initial_time: initial time in form of a datetime object
        :param total_time: maximum time in form of a datetime object
        :param delta_t: length of each time step in form of a timedelta object
        :return:
        """
        self.world.set_worldtime(initial_time)
        t = self.world.get_worldtime()

        self.initiate_statistics(t)

        print('Starting simulation...')
        while t < total_time:
            print('%s/%s' % (t, total_time))
            self.world.evolve(delta_t)
            t = self.world.get_worldtime()
            self.append_statistics(t)

        self.make_report()
        return


    def make_report(self):
        """

        :return: creates all the reports after the simulation
        """
        self.populations.to_csv(self.filename, index_label='t')
        if (self.report_statistics_new_cases):
            self.statistics.to_csv("statistics.csv", index_label='t')
        if (self.report_contacts_flag):
            self.contacts.to_csv("contacts.csv", index_label='t')
        if(self.age_stratified_ICU_Hospital):
            self.age_infection_report_icu['total'] = self.age_infection_report_icu.sum(axis=1)
            self.age_infection_report_hospital['total'] = self.age_infection_report_hospital.sum(axis=1)
            self.age_infection_report_icu.to_csv('ICU-AGE-DIST.csv', index_label='t')
            self.age_infection_report_hospital.to_csv("Hospital-AGE-DIST.csv", index_label='t')
        if (self.print_graphs):
            for node in self.world.get_nodes():
                node_name = node.get_name()
                subdf = self.populations[
                    self.populations.columns[pd.Series(self.populations.columns).str.startswith('%s_' % node_name)]]
                drawplot(subdf, node_name)
            if(self.report_contacts_flag):
                self.contacts.plot(kind='line', use_index=True)
                filename = 'new contacts.png'
                plt.xlabel('Time')
                plt.ylabel('Number of cases')
                plt.savefig(filename)
                plt.close('all')


        totals = pd.DataFrame(index=self.populations.index)
        for status in Status:
            subdf = self.populations[
                self.populations.columns[pd.Series(self.populations.columns).str.endswith('_%s' % status.name)]]
            totals['total_' + status.name] = subdf.sum(axis=1)
        subdf = self.populations[
            self.populations.columns[pd.Series(self.populations.columns).str.endswith('_total')]]
        totals['total_total'] = subdf.sum(axis=1)

        totals.to_csv("total.csv", index_label='t')

        drawplot(totals, "total")
        return

    def append_statistics(self, t):
        """
        appends new statistics at time t.
        :param t: current time
        :return:
        """
        if (self.count_down_to_next_report!=0):
            self.count_down_to_next_report-=1
            return
        p = self.world.get_populations()
        self.populations = self.populations.append(pd.DataFrame(p, index=pd.to_datetime([t])))

        if (self.report_statistics_new_cases):
            s = self.world.get_statistics()
            self.statistics = self.statistics.append(pd.DataFrame(s, index=pd.to_datetime([t])))
        if (self.report_contacts_flag):
            c = self.world.get_all_contacts()
            self.contacts = self.contacts.append(pd.DataFrame(c, index=pd.to_datetime([t])))

        if(self.age_stratified_ICU_Hospital):
            age_infection_report_icu = self.world.get_age_statistics("ICU", Status.Infected)
            age_infection_report_hospital = self.world.get_age_statistics("Hospital", Status.Infected)
            self.age_infection_report_icu = self.age_infection_report_icu.append(
                pd.DataFrame(age_infection_report_icu, index=[t]))
            self.age_infection_report_hospital = self.age_infection_report_hospital.append(
                pd.DataFrame(age_infection_report_hospital, index=[t]))
        self.count_down_to_next_report = self.report_every_n_steps - 1

    def initiate_statistics(self, t):
        """
        creates the dataframes for reporting the statistics
        :param t: initial time
        """
        p = self.world.get_populations()
        self.populations = pd.DataFrame(p, index=[t])

        if (self.report_statistics_new_cases):
            s = self.world.get_statistics()
            self.statistics = pd.DataFrame(s, index=[t])

        if(self.report_contacts_flag):
            c = self.world.get_all_contacts()
            self.contacts = pd.DataFrame(c, index=[t])

        if(self.age_stratified_ICU_Hospital):
            age_infection_report_icu = self.world.get_age_statistics("ICU", Status.Infected)
            age_infection_report_hospital = self.world.get_age_statistics("Hospital", Status.Infected)
            self.age_infection_report_icu = pd.DataFrame(age_infection_report_icu, index=pd.to_datetime([t]))
            self.age_infection_report_hospital = pd.DataFrame(age_infection_report_hospital, index=pd.to_datetime([t]))
        self.count_down_to_next_report = self.report_every_n_steps - 1
