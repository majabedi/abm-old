# ABM COVID-19
(Version 1.0.0)

## Overview
ABM COVID-19 is a Python software to simulate the spread of COVID-19 infection in a prototypical human society. This project uses an Agent Based Modelling (ABM) framework to study the dynamics of infection in an interacting population. Each agent is a single person which can migrate and carry the infection of get infected by other people. The purpose of this project is to study the effect of different measures which are applied in time, on the spread of the infection.

## Download
The code can be downloaded from the [gitlab repository](https://gitlab.com/simm/covid19/abm).

## Requirements
To run the code python3+ should be installed. Therefore it is not dependent on the operating system. The other required packages are:
- [numpy](https://numpy.org/)
- [pandas](https://pandas.pydata.org/)
- [matplotlib](https://matplotlib.org/)

## Basic assumptions
The main assumption is that all the dynamics can be categorized under one of the two types i) the infection dynamics and ii) migration dynamics. 
Each person has one infection status among these states:
  * Susceptible
  * Infected
  * Carrier
  * Recovered (from Infected)
  * Recovered (from Carrier)
  * Dead

And at each time point every agent is on one node. More details of the used model could be found in the [pdf file.](https://gitlab.com/simm/covid19/abm/-/blob/master/Supplementary_Notes_for_the_Agent_Based_Model.pdf)

## How to
Once you have downloaded the files and installed all the requirement you can run the first simulation. You just need to run the following command in the downloaded directory:
```bash
python3.5 abm.py
```
by default it opens the param.in file and reads the parameters from there, unless the parameter file is explicitly given:
```bash
python3.5 abm.py parameter_file.in
```
you can of course change the python3.5 to your own version.

The code gets several input files and runs the simulation with the provided parameters:
### parameter file (default file is param.in)
This parameter file is the main source for parameters. The parameters are defined in the format:
```python
parameter = value
```
all the comments are made by # character and all the characters following the # will be ignored. The following parameters should be defined with the following names:
* *start_date*
is the start date of simulation in the format (YYYY-MM-DD) like 2019-12-01.
* *delta_t*
is the time step of simulation in minutes.
* *total_time*
is the duration of simulation in days.
* *network_file*
is the path of the file which gives the details of migration network of the city.
* *age_distribution_file*
is the path of the file which includes the age distribution.
* *incubation_time,w_sc_i,w_sc_c,w_ir,w_id,w_rs,w_ci,w_cr*
are the parameters which define the infection dynamics of the model according to the [details](https://gitlab.com/simm/covid19/abm/-/blob/master/Supplementary_Notes_for_the_Agent_Based_Model.pdf).
* *print_graphs*
is a boolean which defines whether it should print the produced graphs as png file or not.
* *report_contacts*
is a boolean which defines whether simulator should report new contacts in the whole network over time as a csv file.
* *report_age_stratified_ICU_Hospital*
is a boolean which defines whether simulator should report the number of infected people by their age as a csv file or not.
* *report_statistics_new_cases*
is a boolean which defines whether simulator should report number of newly created cases of each subpopulation in different nodes or not.
* *report_every_n_steps*
is the frequency of reports, every how many steps a report on the above quantities should be made.



### network file
The network file is given as a formatted JSON text and one node of the should be of the form:
```
"Hospital":{
      "capacity":5000,
      "initial_total":1000,
      "initial_infected":10,
      "initial_carrier":1,
      "death_rate_reduction_factor":0.5,
      "T_mu":72.0,
      "T_sigma":24.0,
      "transmission_reduction_factor":0.25
   },
```
* *capacity* is simply capacity og the node.
* *initial_total* is total population size initially assigned to the node.
* *initial_infected* is the number of people who are infected are are in the node.
* *initial_carrier* is the number of people who are carriers of the virus and are in the node. To see the details of difference between Carrier and Infected see the introduction.pdf file.
* *death_rate_reduction_factor* is the change of death rate, in the node. This will be multiplied by w_id, and be used for this node.
* *T* is minimum time required for an agent to stay on this node. (in hours)
* *T_mu, T_sigma* are mean value and standard deviation of minimum time required for an agent to stay on this node. The minimum time is chosen from a log-normal distribution. These parameters will override the T value, if both are given. (in hours)
* *initial_infected* is the number of people who are infected are are in the node.
* *transmission_reduction_factor* is the change of rate of transmitting the virus from infected to susceptible ones, in this node so it will be multiplied by w_sc_i. This parameter is intended to make a difference between different nodes regarding
the transmitting infection.

### age distribution file
This file simply provides the distribution of different ages as a two column csv file. The first column gives the age and the second one gives the normalized cumulative distribution function. The current file is for city [Braunschweig](https://www.citypopulation.de/en/germany/niedersachsen/braunschweig/03101000__braunschweig/)

## Example
You can run the code with the parameters given in the examples folder. It runs a quick simulation for a city of 1000 people for 4 months starting from February 2020. The results will look like the figures below. 
![](example/total.png) "Total population of the city"
![](example/Hospital.png) "Total infected people in node Hospital"
![](example/ICU.png) "Total infected people in node ICU"


## History
The project is started in March 2020 to answer the rising questions of COVID-19 pandemic.


## License
[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)

