# Created by Majid Abedi(majabedi@gmail.com) on 14/03/2020
# This file is intended to create the object to store the statistics of a common place, called node.
# The agents are assigned to a node at all the rimes and all the agents on one node share the same statistics.

import random

from person import Status


class node:
    """
        A class used to store all properties for one node and the possible markov transitions.
    """

    def __init__(self, name, capacity, duration, max_allowed_duration, markov_parameters, transmission_reduction_factor, Susceptible=0, Infected=0, Carrier=0, Recovered_C=0, Recovered_I=0 ,Dead=0, death_rate_reduction_factor = 1 ):
        """
        initializes the object
        :param name: name of the node as string
        :param capacity: capacity of the node
        :param duration: minimum duration required for agents to stay on this node in days
        :param max_allowed_duration: maximum time that agents can spend on this node in days
        :param markov_parameters: markov transition rates
        :param transmission_reduction_factor: ho much the infection transmission should reduce in the this node
        :param Susceptible: number of Susceptible agents
        :param Infected: number of Infected agents
        :param Carrier: number of Carrier agents
        :param Recovered_C: number of Recovered agents from Carrier state
        :param Recovered_I: number of Recovered agents from Infected state
        :param Dead: number of Dead agents
        :param death_rate_reduction_factor: how much the death rate should reduce in this node
        """
        self.name = name
        sub_populations = {Status.Susceptible: Susceptible,
                           Status.Infected: Infected,
                           Status.Carrier: 0,
                           Status.Recovered_C: Recovered_C,
                           Status.Recovered_I: Recovered_I,
                           Status.Dead: Dead,
                           Status.Exposed: Carrier}
        self.size = Susceptible + Infected + Carrier + Recovered_C + Recovered_I + Dead
        self.sub_populations = sub_populations
        self.capacity = capacity
        self.w_sc_i = markov_parameters['w_sc_i']
        self.w_sc_c = markov_parameters['w_sc_c']
        self.w_ir = markov_parameters['w_ir']
        self.w_id = markov_parameters['w_id']
        self.w_rs = markov_parameters['w_rs']
        self.w_ci = markov_parameters['w_ci']
        self.w_cr = markov_parameters['w_cr']
        self.time_duration = duration
        self.transmission_reduction_factor = transmission_reduction_factor
        self.death_rate_reduction_factor = death_rate_reduction_factor
        self.calculate_transition_rate_matrix()
        self.reset_statistics()
        self.max_allowed_duration = max_allowed_duration
        return

    def get_name(self):
        """

        :return: the name of node
        """
        return self.name

    def get_size(self):
        """

        :return: the whole population size
        """
        return self.size

    def get_subpopulation(self, status):
        """

        :param status: status
        :return: the number of agents
        """
        return self.sub_populations[status]

    def get_statistics(self, status):
        """
        returns the statistics of new cases of one status
        :param status: status
        :return: the statistics of new cases
        """
        return self.statistics[status]

    def get_time_duration(self):
        """
        returns the minimum time to stay in the node
        :return: minimum time
        """
        return self.time_duration

    def get_max_allowed_duration(self):
        """
        returns the maximum time to allow an agent stay in the node
        :return: maximum allowed time
        """
        return self.max_allowed_duration

    def one_changes_status(self, old_status, new_status):
        """
        inform the node that one agent has changed the infection status
        :param old_status: old status
        :param new_status: new status
        :return:
        """
        self.sub_populations[old_status] -= 1
        self.sub_populations[new_status] += 1
        if(old_status!=new_status):
            self.statistics[new_status] += 1
        if (self.sub_populations[old_status] < 0):
            raise RuntimeError('population of ' + old_status.name + ' got negative in node ' + self.get_name())
        if (self.sub_populations[new_status] > self.capacity):
            raise RuntimeError('population of ' + new_status.name + ' got more than capacity in node ' + self.get_name())
        # TODO. Consider it once we have different time scales.
        return

    def change_one_subpopulation_and_total(self, status, delta):
        """
        if one agent has migrated from/to the node the statustics should change.
        :param status: the status
        :param delta: change in the corresponding population
        :return:
        """
        self.sub_populations[status] += delta
        self.size += delta
        #This step might be possible to do once after all migrations
        # TODO. Consider it once we have different time scales.
        # self.calculate_transition_rate_matrix()
        return

    def next_markov_state(self, from_status, delta_t):
        """
        produces a random variable and gives the next markov state according to the time step and the markov rates
        :param from_status: the current infection status
        :param delta_t: the time step in days
        :return: the next infection status
        """
        #Transition Rates are independent of delta_t
        transition_rates = self.transition_matrix[from_status].copy()

        for to_status in sorted(list(transition_rates.keys()), key=lambda x:x.value):
            transition_rates[to_status] *= delta_t

        transition_rates[from_status]+=1.

        w = random.uniform(0.0, 1.0)
        cumsum = 0



        for to_status in sorted(list(transition_rates.keys()), key=lambda x:x.value):
            cumsum += transition_rates[to_status]
            if (w < cumsum):
                return to_status
        if (cumsum > 1):
            raise RuntimeError("Cumulative sum of transition probabilities for " + from_status.name + " bigger than 1.")

        return

    def reset_statistics(self):
        """
        resets the statistics
        :return:
        """
        self.statistics = {Status.Susceptible: 0,
                      Status.Infected: 0,
                      Status.Carrier: 0,
                      Status.Recovered_C: 0,
                      Status.Recovered_I: 0,
                      Status.Dead: 0,
                      Status.Exposed: 0}
        return

############################################################
    def calculate_transition_rate_matrix(self):
        """
        In this function we define the markov transitions for the node.
        This function is called from person.py
        Note that here only the transition to other states are considered and not to self,
        and it is not dependent of time step.

        :return:
        """
        matrix = {}
        w_sc_i = self.w_sc_i
        w_sc_c = self.w_sc_c
        w_ir = self.w_ir
        w_id = self.w_id
        w_rs = self.w_rs
        w_ci = self.w_ci
        w_cr = self.w_cr

        # rate of Susceptible to Carrier
        total = self.get_size()
        #To avoid division by zero
        if(total==0):
            total=1
        rate_sc = w_sc_i * float(self.get_subpopulation(Status.Infected)) / total +\
            w_sc_c * float(self.get_subpopulation(Status.Carrier)) / total

        rate_sc *= self.transmission_reduction_factor

        # rate of Infected to Recovered
        rate_ir = w_ir

        # rate of Infected to Dead
        rate_id = w_id

        rate_id*=self.death_rate_reduction_factor

        # rate of Recovered to Susceptible
        rate_rs = w_rs

        # rate of Carrier to Recovered
        rate_cr = w_cr

        # rate of Carrier to Infected
        rate_ci = w_ci

        matrix[Status.Susceptible] = {
            Status.Carrier: rate_sc,
            Status.Susceptible: - rate_sc
        }

        matrix[Status.Carrier] = {
            Status.Infected: rate_ci,
            Status.Recovered_C: rate_cr,
            Status.Carrier: - rate_ci - rate_cr
        }

        matrix[Status.Infected] = {
            Status.Infected: - rate_ir - rate_id,
            Status.Recovered_I: rate_ir,
            Status.Dead: rate_id
        }

        matrix[Status.Recovered_C] = {
            Status.Recovered_C: - rate_rs,
            Status.Susceptible: rate_rs,
        }

        matrix[Status.Recovered_I] = {
            Status.Recovered_I: - rate_rs,
            Status.Susceptible: rate_rs,
        }

        matrix[Status.Dead] = {
            Status.Dead: 0
        }

        self.transition_matrix = matrix

        return
