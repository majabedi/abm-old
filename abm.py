# Created by Majid Abedi(majabedi@gmail.com) o 12/03/2020
# This file handles the main runner of the simulation. It connects to parameter reader and sets
# up a simulator to run.

import random
import sys
from datetime import datetime, timedelta

import numpy.random as nprand

import Simulator
from parameter_reader import read_parameters


def main():
    # The default parameter file
    parameter_file = "param.in"
    if (len(sys.argv) > 1):
        parameter_file = sys.argv[1]
    print('Running the simulation with parameter file: %s' % (parameter_file))

    # Create a map of parameters and values
    parameters = read_parameters(parameter_file)

    # The specifications of population:
    population_parameters = {
        'incubation_time': float(parameters['incubation_time']),
        'age_distribution_file': parameters['age_distribution_file']
    }

    # The parameters to calculate rates of markov transition rates.
    markov_parameters = {
        'w_sc_i': float(parameters['w_sc_i']),
        'w_sc_c': float(parameters['w_sc_c']),
        'w_ir': float(parameters['w_ir']),
        'w_id': float(parameters['w_id']),
        'w_rs': float(parameters['w_rs']),
        'w_ci': float(parameters['w_ci']),
        'w_cr': float(parameters['w_cr']),
    }
    delta_t = timedelta(minutes=int(parameters['delta_t']))
    initial_date=datetime.strptime(parameters['start_date'], '%Y-%m-%d')
    total_time_duration = initial_date + timedelta(days=int(parameters['total_time']))

    network_file = parameters['network_file']

    if('seed' in parameters.keys()):
        seed = int(parameters['seed'])
        random.seed(seed)
        nprand.seed(seed)

    simulator = Simulator.simulator()
    simulator.with_output('out.csv')
    simulator.initialize_network_explicit_nodes(network_file = network_file, population_parameters=population_parameters ,markov_parameters=markov_parameters )
    if (parameters['print_graphs'].lower() == 'true'):
        simulator.print_graphs_at_the_end()
    if (parameters['report_contacts'].lower() == 'true'):
        simulator.report_contacts()
    if (parameters['report_age_stratified_ICU_Hospital'].lower() == 'true'):
        simulator.report_stratified_ICU_Hospital()
    if (parameters['report_statistics_new_cases'].lower() == 'true'):
        simulator.report_new_cases()
    simulator.set_report_every_n_steps(int(parameters['report_every_n_steps']))


    simulator.run(initial_date,total_time_duration, delta_t)
    return


if __name__ == "__main__":
    main()
