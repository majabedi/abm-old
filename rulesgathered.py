# Created by Anastasios Siokis(anastasiossio@gmail.com) on 24/03/2020
# This file is intended to include all the rules which are applied to the agents to migrate.

import random
from datetime import datetime
import numpy as np

from person import Status

lockdown_school_date=d=datetime(2020, 2, 10)
lockdown_school_se_date=d=datetime(2020, 2, 15)
lockdown_school_se_wp_date=d=datetime(2020, 2, 20)
lockdown_school_se_wp_sm_date=d=datetime(2020, 2, 25)
elderly_stay_home_date=d=datetime(2020, 2, 27)


#########################################
# Rule 1.
# #The people <=20 or >=65 do not go the Workplace and people >=65 do not go to School node
# #The people between 20 and 65 can go everywhere exept for school
# We don't have the Hospital because only infected people should go there
# In this way we don't need rule 3....right?
def rule1(person,t,delta_t):
    rule_applied = False
    destination = person.get_node()
    lockdown = True
    lockdown_school = True
    lockdown_school_se = True
    lockdown_school_se_wp = True
    lockdown_school_se_wp_sm = True
    possible_destination_20  =["Home", "Super Market", "School", "Social Event"]
    possible_destination_65  =["Home", "Super Market", "Social Event"]
    possible_destination_rest=["Home", "Super Market", "Work Place", "Social Event"]
    if (lockdown):
        if (t>lockdown_school_date and lockdown_school):
            possible_destination_20.remove("School")
        if (t>lockdown_school_se_date and lockdown_school_se):
            possible_destination_20.remove("Social Event")
            possible_destination_65.remove("Social Event")
            possible_destination_rest.remove("Social Event")
        if (t>lockdown_school_se_wp_date and lockdown_school_se_wp):
            possible_destination_rest.remove("Work Place")
        if (t>lockdown_school_se_wp_sm_date and lockdown_school_se_wp_sm):
            possible_destination_20.remove("Super Market")
            possible_destination_65.remove("Super Market")
            possible_destination_rest.remove("Super Market")            
    if (person.get_node().get_name() == "Hospital" or person.get_node().get_name() == "ICU"):
        return rule_applied, destination
    if(person.get_age()<=20.0):
        destination = random.choice(possible_destination_20)
        rule_applied = True
    else:
        if (person.get_age()>=65.0):
            destination = random.choice(possible_destination_65)
            rule_applied = True
        else:
            if (person.get_age()>20.0 and person.get_age()<65.0):
                destination = random.choice(possible_destination_rest)
                rule_applied = True
    return rule_applied, destination

#########################################
# Rule 2.
# #The people who are Infected go directly to hospital
def rule2(person,t,delta_t):
    #print("rule 2 called")
    rate_of_going_hospital_old=0.11
    rate_of_going_hospital_young=0.02
    rule_applied = False
    destination = person.get_node()
    if(person.get_status()==Status.Infected and person.get_node().get_name() != "Hospital" ):
        w=random.uniform(0.,1.)
        dummy_rate=rate_of_going_hospital_young+((person.get_age()-5.)*(rate_of_going_hospital_old-rate_of_going_hospital_young)/75)
        if(w<dummy_rate*delta_t):
            destination = 'Hospital'
            rule_applied = True
    return rule_applied, destination
# END Rule 2
#########################################

#########################################
# Rule 4.
# The recovered should leave the hospital immediately
def rule4(person,t,delta_t):
    rule_applied = False
    destination = person.get_node()
    if ((person.get_status() == Status.Recovered_I or person.get_status() == Status.Recovered_C)\
            and person.get_node().get_name() == "Hospital"):
        destination = "Home"
        rule_applied = True
    return rule_applied, destination
#End Rule 4
#########################################

# Rule 7.
# The recovered should leave the ICU immediately to hospital
def rule7(person,t,delta_t):
    rule_applied = False
    destination = person.get_node()
    if ((person.get_status() == Status.Recovered_I or person.get_status() == Status.Recovered_C) \
            and person.get_node().get_name() == "ICU"):
        destination = "Hospital"
        rule_applied = True
    return rule_applied, destination
# End Rule 7
#########################################

# Rule 9.
# The current node is Home the older people should go outside less according to the parameter
def rule9(person,t,delta_t):
    rule_applied = False
    destination = person.get_node()
    elderly_stay_home = True
    w = random.uniform(0.9, 1.) * person.get_age()
    partial_lockdown_threshold = 80
    if(t>elderly_stay_home_date and elderly_stay_home):
        if(person.get_age()>=65 and w>partial_lockdown_threshold):
            destination = "Home"
            rule_applied = True
    return rule_applied, destination
# END Rule 9
#########################################

#########################################
# Rule 6.
# The people go to ICU if they need critical care depending on their status

def rule6(person,t,delta_t):
    rule_applied = False
    destination = person.get_node()
    if (person.get_status() == Status.Infected and \
            person.get_node().get_name() == "Hospital" and \
            person.get_time_passed_current_node() > np.random.exponential(3)):
        w = random.uniform(0.0, 1.0)
        #                0 - 40: 0.05
        if (person.get_age() <= 40 and w < 0.05 * delta_t):
            destination = 'ICU'
            rule_applied = True
        #                40 - 60: 0.08
        else:
            if (person.get_age() <= 60 and w < 0.08 * delta_t):
                destination = 'ICU'
                rule_applied = True
            #                    61 - 75: 0.15
            else:
                if (person.get_age() <= 75 and w < 0.15 * delta_t):
                    destination = 'ICU'
                    rule_applied = True
                    #   75+: 0.225
                else:
                    if (person.get_age() > 75 and w < 0.225 * delta_t):
                        destination = 'ICU'
                        rule_applied = True
    return rule_applied, destination
# END Rule 6
#########################################