# Created by Majid Abedi(majabedi@gmail.com) on 25/03/2020
# This file is intended to have a node which is individualized, to prevent the agents share a common place
# This is very important when the lock down is applied and the agents go to their individual homes.

from node import node

class node_individualized(node):
    """
       A class used to store the individualized nodes. It inherits all properties of node object and has one prent node.
       The parent node hold the global statistics.
       For example, each individualized home is one object of node_individualized and they have a common parent node.
       Since it does not make sense to store the each individual home statistics, we store all of them in the parent node.
    """

    def __init__(self, global_node, name, capacity, duration, max_allowed_duration, markov_parameters, transmission_reduction_factor, Susceptible=0, Infected=0, Carrier=0, Recovered_I=0, Recovered_C=0 ,Dead=0, death_rate_reduction_factor = 1 ):
        """
        initializes the object

        :param global_node: the parent node to store the global statistics
        :param name: name of the node as string
        :param capacity: capacity of the node
        :param duration: minimum duration required for agents to stay on this node in days
        :param max_allowed_duration: maximum time that agents can spend on this node in days
        :param markov_parameters: markov transition rates
        :param transmission_reduction_factor: ho much the infection transmission should reduce in the this node
        :param Susceptible: number of Susceptible agents
        :param Infected: number of Infected agents
        :param Carrier: number of Carrier agents
        :param Recovered_C: number of Recovered agents from Carrier state
        :param Recovered_I: number of Recovered agents from Infected state
        :param Dead: number of Dead agents
        :param death_rate_reduction_factor: how much the death rate should reduce in this node
        """
        self.global_node = global_node
        super().__init__(name, capacity, duration, max_allowed_duration, markov_parameters,
                     transmission_reduction_factor, Susceptible=Susceptible, Infected=Infected, Carrier=Carrier, Recovered_I=Recovered_I, Recovered_C=Recovered_C, Dead=Dead,
                     death_rate_reduction_factor=death_rate_reduction_factor)

    def one_changes_status(self, old_status, new_status):
        """
        to inform the node that one agent has changed the infection status. It also changes the status in the parent node.
        :param old_status: old status
        :param new_status: new status
        :return:
        """
        self.global_node.one_changes_status(old_status,new_status)
        super().one_changes_status(old_status,new_status)
        return

    def change_one_subpopulation_and_total(self, status, delta):
        """
        to inform the node that one agent has migrated from/to the node. it also informs the parent node.
        :param status: the infection status of the migrating agent
        :param delta: the change.
        :return:
        """
        self.global_node.change_one_subpopulation_and_total(status, delta)
        super().change_one_subpopulation_and_total(status, delta)
        return


