# Created by Majid Abedi(majabedi@gmail.com) on 12/03/2020
# This file is intended to create the whole modelled world. It contains all tje nodes, all the agents and also the time.

import random
from collections import OrderedDict

import numpy as np

from all_rules import rules
from node import node
from node_individualized import node_individualized
from person import Status
from person import person


def create_complete_graph(nodes):
    """
    creates a complete graph of nodes
    :param nodes: nodes.
    :return:
    """
    g = {}
    for n in nodes:
        edges = nodes.copy()
        edges.remove(n)
        g[n] = edges
    return g

class world:
    """
        A class used to store all properties of the simulated world. It includes agents and nodes.
    """

        # initialize a network
    def initialize_network_small_world(self, number_of_nodes=10, size=10, infection_p=0.1, k_over_2=2, beta=0.025, migration_rate = 0.1):
        """
        This method is not used anymore.
        create a network with small world connection graph
        :param number_of_nodes: number of nodes
        :param size: initial population size in each node
        :param infection_p: probability if being infected for each node
        :param k_over_2: the parameter for small world network
        :param beta: the parameter for small world network
        :param migration_rate: rate of migration
        :return:
        """
        from smallworld import get_smallworld_graph
        nodes = []
        persons = []
        print('Initializing the network...')
        for i in range(number_of_nodes):
            infected = np.random.binomial(size, infection_p)
            new_node = node(name='%d' % i, Infected=infected, Susceptible=size - infected, capacity=size)
            print('Created node %s with total %d people, and %d infected.' % (i, size, infected))
            for j in range(infected):
                persons.append(person(Status.Infected, new_node))
            for j in range(size - infected):
                persons.append(person(Status.Susceptible, new_node))
            nodes.append(new_node)
        self.nodes = nodes
        self.persons = persons
        self.connection_graph = get_smallworld_graph(number_of_nodes, k_over_2, beta)
        self.migration_rate = migration_rate
        return

    def initialize_network_complete_graph(self, nodes, size=10, infection_p=0.1, c=100, migration_rate = 0.1):
        """

        :param number_of_nodes: number of nodes
        :param size: initial population size in each node
        :param infection_p: probability if being infected for each node
        :param c: capacity of each node
        :param migration_rate: rate of migration
        :return:
        """
        dummy_nodes = []
        persons = []
        print('Initializing the network...')
        for n in nodes:
            infected = np.random.binomial(size, infection_p)
            new_node = node(name=n, Infected=infected, Susceptible=size - infected, capacity=c)
            print('Created node %s with total %d people, and %d infected.' % (n, size, infected))
            for j in range(infected):
                persons.append(person(Status.Infected, new_node))
            for j in range(size - infected):
                persons.append(person(Status.Susceptible, new_node))
            dummy_nodes.append(new_node)
        self.nodes = dummy_nodes
        self.persons = persons
        self.connection_graph = create_complete_graph(self.nodes)
        self.migration_rate = migration_rate

        return


    def evolve(self, delta_t):
        """
        world evolves for timestep delta_t
        :param delta_t: time step in format of deltatime object
        :return:
        """
        self.reset_all_nodes_statistics()
        dt=delta_t.seconds/86400.
        self.interact_infect(dt)
        self.migrate(dt)
        self.calculate_all_transition_rate_matrices()
        self.worldtime+=delta_t
        return

    def interact_infect(self, delta_t):
        """
        evolves the infection in all the individuals
        :param delta_t: time step in days
        :return:
        """
        for p in random.sample(self.persons, len(self.persons)):
            p.interact_with_population(delta_t)
        return

    def calculate_all_transition_rate_matrices(self):
        """
        calculate the markov transition rates, because some of them are dependent on the different populations.
        """
        for n in self.nodes:
            n.calculate_transition_rate_matrix()

    def reset_all_nodes_statistics(self):
        """
        just counts the different populations
        """
        for n in self.nodes:
            n.reset_statistics()
        return


    def get_populations(self):
        """
        returns a different types of infections and their populations on the node
        :return: returns a dictionary
        """
        populations = {}
        for n in self.nodes:
            for status in Status:
                populations['%s_%s' % (n.get_name(), status.name)] = n.get_subpopulation(status)
            populations['%s_total' % (n.get_name())] = n.get_size()
        return populations


    def get_statistics(self):
        """
        returns the statistics of different new cases in different nodes
        :return: returns the statistics
        """
        statistics = {}
        for n in self.nodes:
            for status in Status:
                statistics['%s_%s' % (n.get_name(), status.name)] = n.get_statistics(status)
        return statistics

    def get_nodes(self):
        """
        returns the nodes
        :return: nodes
        """
        return self.nodes

    def get_node(self,name):
        """
        returns a object node
        :param name: string with the name of the node
        :return: the node object
        """
        for n in self.nodes:
            if (n.get_name()==name):
                return n
        return None


    def get_connection_graph(self):
        """
        returns the connection graph of the world
        :return: connection graph object
        """
        return self.connection_graph


    def get_age_statistics(self,node_name,status):
        """
        returns the statistics for a specific node and status
        :param node_name: name of the node
        :param status: status
        :return: the statistics
        """
        statistics = {}
        for i in np.arange(5,85,5):
            statistics[i]=0
        for p in self.persons:
            if(p.get_node().get_name()==node_name and p.get_status()==status):
                if (p.get_age() in statistics.keys()):
                    statistics[p.get_age()] += 1
                else:
                    statistics[p.get_age()] = 1
        return statistics

    def get_all_contacts(self):
        """
        returns all contacts in all nodes
        :return: a dictionary of nodes and their personal contacts
        """
        list_of_nodes = [n.get_name() for n in self.get_nodes()]
        contacts={}
        for n in list_of_nodes:
            contacts[n]=self.get_contacts(n)
        return contacts

    def get_contacts(self,node_name):
        """
        get the new contacts on a single node
        :param node_name: node name
        :return: the number of new contacts
        """
        contacts = 0
        #Add the exception to make it faster
        #Todo: do it in a proper way

        if (node_name!='Home'):
            n=self.get_node(node_name).get_size()
            return int(n*(n-1)/2)
        for p in self.persons:
            if(p.get_node().get_name()==node_name):
                contacts+=p.get_node().get_size()-1
        return int(contacts/2)

    def set_worldtime(self,t):
        """
        sets the current time of the world
        :param t: time object
        :return:
        """
        self.worldtime = t
        return

    def get_worldtime(self):
        """
        returns the time of the simulation
        :return: the time object
        """
        return self.worldtime


    def migrate(self,delta_t):
        """
        do the migration for all agents for the given time step
        :param delta_t: time step
        :return:
        """
        #p is the person chosen from the whole population randomely
        for p in random.sample(self.persons, len(self.persons)):

            #Here we apply the rules
            #For now we have only one rule defined in another file.

            if (p.get_time_passed_current_node() < p.get_node().get_time_duration()):
                p.increase_time_passed_current_node(delta_t)
                continue

            for rule in rules:

                rule_applied, destination_node_name = rule(person=p,t=self.get_worldtime(),delta_t=delta_t)
                if(rule_applied):
                    #if(rule.__name__ != "rule1"):
                    #   print('Rule %s is applied'%(rule.__name__))
                    #print('Rule %s is applied'%(rule.__name__))
                    if(destination_node_name in p.get_individualized_nodes().keys()):
                        destination = p.get_individualized_nodes()[destination_node_name]
                    else:
                        destination=self.get_node(destination_node_name)
                    if (destination is not None):
                        p.migrate_to(destination)
                    continue
            p.increase_time_passed_current_node(delta_t)
        return


    def initialize_network_complete_graph_from_file_individualized_home(self, network_file,
                                                                        population_parameters,
                                                                        markov_parameters):
        """
        initializes the network for the world from a file
        :param network_file: the network file
        :param population_parameters: parameters for population
        :param markov_parameters: parameters for markov processes
        :return:
        """
        import json
        _network = json.load(open(network_file), object_pairs_hook=OrderedDict)
        nodes = _network.keys()
        dummy_nodes = []
        persons = []
        print('Initializing the network...')
        incubation_time = population_parameters['incubation_time']
        for n in nodes:
            if (n.startswith("-")):
                continue
            print('Reading node ' + n + ' from file...')
            if (_network[n]["initial_total"] is not None):
                initial_population = _network[n]["initial_total"]
            else:
                raise RuntimeError("The initial_total for this node is not given. I will stop.")

            if (_network[n]["capacity"] is not None):
                c = _network[n]["capacity"]
            else:
                raise RuntimeError("The capacity for this node is not given. I will stop.")

            if ("initial_infected" in _network[n].keys()):
                infected = _network[n]["initial_infected"]
            else:
                print("The initial_infected persons on this node is not given. I will consider a binomial distribiotn.")
                infected = np.random.binomial(initial_population, infection_p)
            carrier=0
            if ("initial_carrier" in _network[n].keys()):
                carrier = _network[n]["initial_carrier"]
            transmission_reduction_factor = 1.
            if ("transmission_reduction_factor" in _network[n].keys()):
                transmission_reduction_factor = _network[n]["transmission_reduction_factor"]
            death_rate_reduction_factor = 1.
            if ("death_rate_reduction_factor" in _network[n].keys()):
                death_rate_reduction_factor = _network[n]["death_rate_reduction_factor"]
            duration = 0
            if ("T" in _network[n].keys()):
                duration = _network[n]["T"] / 24.
            if (n == 'ICU' or n == 'Hospital'):
                duration = np.random.exponential(duration)
            #Put a lognormal distribution for the times staying in a node
            if ("T_mu" in _network[n].keys() and "T_sigma" in _network[n].keys()):
                duration = np.random.lognormal(_network[n]["T_mu"] / 24., _network[n]["T_sigma"] / 24.)

            # basically infinite
            max_allowed_duration = 1000000
            if ("max_allowed_duration" in _network[n].keys()):
                max_allowed_duration = _network[n]["max_allowed_duration"] / 24.
                # TODO should be converted to days
            age_dist = np.genfromtxt(population_parameters['age_distribution_file'], delimiter=',', skip_header=1)
            if (n == "Home"):

                new_parent_node = node(name=n, duration=duration, max_allowed_duration=max_allowed_duration,
                                       transmission_reduction_factor=transmission_reduction_factor, Infected=infected,
                                       Susceptible=initial_population - infected - carrier, Carrier=carrier, capacity=c,
                                       markov_parameters=markov_parameters,
                                       death_rate_reduction_factor=death_rate_reduction_factor)

                samples = random.sample(range(initial_population), infected + carrier)
                carrier_people = samples[0:carrier]
                infected_people = samples[carrier:carrier + infected]
                # number of already allocated people
                j = 0
                while j < initial_population:

                    # create initial population of the home with mean of 1.7 and minimum 1 person
                    house_population = np.random.binomial(5, 0.14) + 1
                    if (j + house_population) > initial_population:
                        house_population = initial_population - j

                    carriers_current_home = 0
                    infected_current_home = 0
                    for ip in range(house_population):
                        j += 1
                        if (j in carrier_people):
                            carriers_current_home += 1
                        if (j in infected_people):
                            infected_current_home += 1

                    # TODO Fix the capacity
                    new_node = node_individualized(name='Home', global_node=new_parent_node, duration=duration,
                                                   max_allowed_duration=max_allowed_duration,
                                                   transmission_reduction_factor=transmission_reduction_factor,
                                                   Infected=infected_current_home,
                                                   Susceptible=house_population - infected_current_home - carriers_current_home,
                                                   Carrier=carriers_current_home, capacity=c,
                                                   markov_parameters=markov_parameters,
                                                   death_rate_reduction_factor=death_rate_reduction_factor)

                    print('Created node %s with total %d people,%d infected, %d carrier, and capacity %d.' % (
                        n, house_population, infected_current_home, carriers_current_home, c))
                    j -= house_population
                    for ip in range(house_population):
                        j += 1
                        status = Status.Susceptible
                        if (j in carrier_people):
                            status = Status.Exposed
                        if (j in infected_people):
                            status = Status.Infected
                        rnd = random.uniform(0, 1)
                        age = age_dist[np.searchsorted(age_dist[:, 1] > rnd, True), 0]
                        p = person(status, new_node, incubation_time, age)
                        p.set_individualized_nodes(
                            {
                                'Home': new_node
                            }
                        )
                        persons.append(p)

                    new_node.reset_statistics()
                dummy_nodes.append(new_parent_node)

            else:
                new_node = node(name=n, duration=duration, max_allowed_duration=max_allowed_duration,
                                transmission_reduction_factor=transmission_reduction_factor, Infected=infected,
                                Susceptible=initial_population - infected - carrier, Carrier=carrier, capacity=c,
                                markov_parameters=markov_parameters,
                                death_rate_reduction_factor=death_rate_reduction_factor)
                print('The time duration to stay in %s is %f' % (n, duration))
                print('Created node %s with total %d people,%d infected, %d carrier, and capacity %d.' % (
                    n, initial_population, infected, carrier, c))
                for j in range(infected):
                    rnd = random.uniform(0, 1)
                    age = age_dist[np.searchsorted(age_dist[:, 1] > rnd, True), 0]
                    p = person(Status.Infected, new_node, incubation_time, age)
                    p.set_time_passed_current_node(random.uniform(0, duration))
                    persons.append(p)
                for j in range(carrier):
                    rnd = random.uniform(0, 1)
                    age = age_dist[np.searchsorted(age_dist[:, 1] > rnd, True), 0]
                    # TODO: Get rid of this Fucking Exception Exposed! It will screw us
                    p = person(Status.Exposed, new_node, incubation_time, age)
                    p.set_time_passed_current_node(random.uniform(0, duration))
                    persons.append(p)
                for j in range(initial_population - infected - carrier):
                    rnd = random.uniform(0, 1)
                    age = age_dist[np.searchsorted(age_dist[:, 1] > rnd, True), 0]
                    p = person(Status.Susceptible, new_node, incubation_time, age)
                    p.set_time_passed_current_node(random.uniform(0, duration))
                    persons.append(p)
                new_node.reset_statistics()
                dummy_nodes.append(new_node)
        self.nodes = dummy_nodes
        self.persons = persons
        self.connection_graph = create_complete_graph(self.nodes)

        return